package info.francescoflorio.lesson10_app1;

import android.app.Application;
import android.content.Context;
import android.support.multidex.MultiDex;

import com.crashlytics.android.Crashlytics;
import com.twitter.sdk.android.Twitter;
import com.twitter.sdk.android.core.TwitterAuthConfig;
import com.facebook.FacebookSdk;

import io.fabric.sdk.android.Fabric;

/**
 * Lesson 10 - App 1
 * <p/>
 * Created by Ing. Francesco Florio on 09/01/16
 * All Right Reserved.
 */
public class CustomApplication extends Application {

    // Note: Your consumer key and secret should be obfuscated in your source code before shipping.
    private static final String TWITTER_KEY = "fLV937D4xBMYJFI0OzdaLD2qr";
    private static final String TWITTER_SECRET = "dr3yhekONJisDh3lFRLC6NaVYOfiLJEB7Fb5TDYtzjDN44GUus";

    @Override protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    @Override public void onCreate() {
        super.onCreate();
        TwitterAuthConfig authConfig = new TwitterAuthConfig(TWITTER_KEY, TWITTER_SECRET);
        Fabric.with(this, new Twitter(authConfig), new Crashlytics());
        FacebookSdk.sdkInitialize(getApplicationContext());
    }

}
