package info.francescoflorio.lesson10_app1;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.facebook.appevents.AppEventsLogger;

import butterknife.Bind;
import butterknife.ButterKnife;
import info.francescoflorio.lesson10_app1.facebook.FacebookActivity;
import info.francescoflorio.lesson10_app1.googleplus.GooglePlusActivity;
import info.francescoflorio.lesson10_app1.twitter.TwitterActivity;


public class MainActivity extends AppCompatActivity {

    @Bind(R.id.list) ListView list;

    private Class< ? extends Activity> activities[] = new Class[]{
                                        FacebookActivity.class,
                                        TwitterActivity.class,
                                        GooglePlusActivity.class};

    @Override protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        ArrayAdapter<String> itemsAdapter = new ArrayAdapter<>(list.getContext(), android.R.layout.simple_list_item_1, getResources().getStringArray(R.array.social));
        list.setAdapter(itemsAdapter);

        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Log.i(" xxx ", "Tap: "+position);
                startActivity(new Intent(view.getContext(), activities[position]));
            }
        });
    }

    @Override protected void onResume() {
        super.onResume();
        // Logs 'install' and 'app activate' App Events.
        AppEventsLogger.activateApp(this);
    }

    @Override protected void onPause() {
        super.onPause();
        // Logs 'app deactivate' App Event.
        AppEventsLogger.deactivateApp(this);
    }
}
