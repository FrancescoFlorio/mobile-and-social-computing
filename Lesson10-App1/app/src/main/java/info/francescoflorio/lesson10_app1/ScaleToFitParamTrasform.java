package info.francescoflorio.lesson10_app1;

import android.graphics.Bitmap;

import com.squareup.picasso.Transformation;

/**
 * Created by F.Florio on 16/09/14.
 * floriofrancesco@gmail.com
 */
public class ScaleToFitParamTrasform implements Transformation {

    public static final boolean WIDTH_PARAM = false;
    public static final boolean HEIGHT_PARAM = true;

    private int paramValue;
    private boolean isHeightScale;

    public ScaleToFitParamTrasform(int paramValue, boolean paramType) {
        this.paramValue = paramValue;
        this.isHeightScale = paramType;
    }

    @Override
    public Bitmap transform(Bitmap source) {
        float scale;
        int newSize;
        Bitmap scaledBitmap;
        if (isHeightScale) {
            scale = (float) paramValue / source.getHeight();
            newSize = Math.round(source.getWidth() * scale);
            scaledBitmap = Bitmap.createScaledBitmap(source, newSize, paramValue, true);
        } else {
            scale = (float) paramValue / source.getWidth();
            newSize = Math.round(source.getHeight() * scale);
            scaledBitmap = Bitmap.createScaledBitmap(source, paramValue, newSize, true);
        }

        if (scaledBitmap != source)
            source.recycle();

        return scaledBitmap;
    }

    @Override public String key() {
        return new StringBuilder("scaleRespectRatio")
                .append(paramValue)
                .append(isHeightScale)
                .toString();
    }
}
