package info.francescoflorio.lesson10_app1.facebook;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import info.francescoflorio.lesson10_app1.R;

/**
 * Lesson 10 - App 1
 * <p/>
 * Created by Ing. Francesco Florio on 09/01/16
 * All Right Reserved.
 */
public class FacebookActivity extends AppCompatActivity {

    @Bind(R.id.login_button) LoginButton loginButton;
    private CallbackManager callbackManager;

    @Override protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_facebook);
        ButterKnife.bind(this);

        callbackManager = CallbackManager.Factory.create();
        loginButton.setReadPermissions(getReadPermissions());
        AccessToken accessToken = AccessToken.getCurrentAccessToken();
        if(accessToken != null) openFacebookInfo();

        // Callback registration
        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override public void onSuccess(LoginResult loginResult) {
                Toast.makeText(getApplicationContext(), "Logged on Facebook!", Toast.LENGTH_SHORT).show();
                openFacebookInfo();
            }

            @Override public void onCancel() {
                Toast.makeText(getApplicationContext(), "Login cancelled!", Toast.LENGTH_SHORT).show();
            }

            @Override public void onError(FacebookException exception) {
                Toast.makeText(getApplicationContext(), "Error: "+exception.getLocalizedMessage()+"!", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    private List<String> getReadPermissions(){
        List<String> readPermissions = new ArrayList<>();
        readPermissions.add("user_friends");
        readPermissions.add("email");
        readPermissions.add("user_posts");
        return readPermissions;
    }

    private void openFacebookInfo(){
        startActivity(new Intent(getApplicationContext(), FacebookInfoActivity.class));
        finish();
    }
}
