package info.francescoflorio.lesson10_app1.facebook;

import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.twitter.sdk.android.core.models.Tweet;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import info.francescoflorio.lesson10_app1.R;
import info.francescoflorio.lesson10_app1.ScaleToFitParamTrasform;

/**
 * Lesson 10 - App 1
 * <p/>
 * Created by Ing. Francesco Florio on 10/01/16
 * All Right Reserved.
 */
public class FacebookAdapter extends BaseAdapter{

    private List<FacebookPost> posts = new ArrayList<>();

    int imageWidth;

    public FacebookAdapter(Resources res){
        imageWidth = res.getDisplayMetrics().widthPixels - (2 * res.getDimensionPixelSize(R.dimen.itemPadding));
    }

    @Override public int getCount() { return posts.size(); }
    @Override public Object getItem(int position) { return posts.get(position); }
    @Override public long getItemId(int position) { return position; }

    @Override public View getView(int position, View convertView, ViewGroup parent) {
        Context ctx = parent.getContext();
        View v = convertView;
        ViewHolder vh;
        if(v == null){
            v = LayoutInflater.from(ctx).inflate(R.layout.adapter_facebook, parent, false);
            vh = new ViewHolder(v);
            v.setTag(vh);
        }vh = (ViewHolder) v.getTag();

        FacebookPost p = posts.get(position);

        if(p.getName() != null){
            vh.title.setVisibility(View.VISIBLE);
            vh.title.setText(p.getName());
        }else vh.title.setVisibility(View.GONE);

        if(p.getDescription() != null){
            vh.description.setVisibility(View.VISIBLE);
            vh.description.setText(p.getDescription());
        }else vh.description.setVisibility(View.GONE);

        if(p.getLink() != null){
            vh.link.setVisibility(View.VISIBLE);
            vh.link.setText(p.getLink());
        }else vh.link.setVisibility(View.GONE);

        if(p.getPicture() != null && !p.getPicture().isEmpty()) {
            vh.image.setVisibility(View.VISIBLE);
            Picasso.with(ctx)
                    .load(p.getPicture())
                    .transform(new ScaleToFitParamTrasform(imageWidth, ScaleToFitParamTrasform.WIDTH_PARAM))
                    .into(vh.image);
        }else vh.image.setVisibility(View.GONE);

        return v;
    }

    public void updateDataset(List<FacebookPost> posts){ this.posts = posts; notifyDataSetChanged(); }

    public class ViewHolder{
        @Bind(R.id.picture) ImageView image;
        @Bind(R.id.title) TextView title;
        @Bind(R.id.description) TextView description;
        @Bind(R.id.link) TextView link;

        public ViewHolder(View v){
            ButterKnife.bind(this, v);
        }
    }
}
