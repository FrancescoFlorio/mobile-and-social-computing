package info.francescoflorio.lesson10_app1.facebook;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import com.facebook.AccessToken;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import info.francescoflorio.lesson10_app1.R;

/**
 * Lesson 10 - App 1
 * Created by Ing. Francesco Florio on 10/01/16
 * All Right Reserved.
 */
public class FacebookInfoActivity extends AppCompatActivity {

    ListView listview;
    @Bind(R.id.userIdTV) TextView userIdTv;
    @Bind(R.id.userNameTv) TextView usernameTv;
    @Bind(R.id.emailTv) TextView emailTv;
    @Bind(R.id.accessTokenTV) TextView accessTokenTv;
    private FacebookAdapter adapter;

    private AccessToken accessToken;

    @Override protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_facebooklist);
        listview = (ListView)findViewById(R.id.list);
        View header = LayoutInflater.from(this).inflate(R.layout
                .activity_facebooklist_header, null);
        listview.addHeaderView(header);
        adapter = new FacebookAdapter(getResources());
        listview.setAdapter(adapter);
        ButterKnife.bind(this, header);

        accessToken = AccessToken.getCurrentAccessToken();
        if(accessToken != null) {
            userIdTv.setText(accessToken.getUserId());
            accessTokenTv.setText(accessToken.getToken());
            updateMe();
            getLatestPosts();
        }



    }

    private void updateMe(){
        GraphRequest request = GraphRequest.newMeRequest(accessToken, new GraphRequest
                .GraphJSONObjectCallback() {
                    @Override public void onCompleted(JSONObject object, GraphResponse response) {
                        Log.i("TAG", "O: " + response.getRawResponse());
                        String username = object.optString("name");
                        String email = object.optString("email");
                        String id = object.optString("id");

                        usernameTv.setText(username);
                        emailTv.setText(email);
                        Log.i("TAG", ">>" + email);
                    }
                });

        Bundle parameters = new Bundle();
        parameters.putString("fields", "id, name, email");
        request.setParameters(parameters);
        request.executeAsync();
    }

    private void getLatestPosts(){
        GraphRequest request = GraphRequest.newGraphPathRequest(
                accessToken,
                "/me/feed",
                new GraphRequest.Callback() {
                    @Override
                    public void onCompleted(GraphResponse response) {
                        if(response == null) return;
                        JSONObject object = response.getJSONObject();
                        JSONArray rawPosts = object.optJSONArray("data");
                        List<FacebookPost> posts = new ArrayList<>(rawPosts.length());
                        for (int i = 0; i < rawPosts.length(); i++)
                            posts.add(new FacebookPost(rawPosts.optJSONObject(i)));
                        adapter.updateDataset(posts);
                    }
                });

        Bundle parameters = new Bundle();
        parameters.putString("fields", "name,full_picture,link,description");
        request.setParameters(parameters);
        request.executeAsync();
    }
}
