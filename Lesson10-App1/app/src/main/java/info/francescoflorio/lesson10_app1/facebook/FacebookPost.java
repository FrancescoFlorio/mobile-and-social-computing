package info.francescoflorio.lesson10_app1.facebook;

import android.util.Log;

import org.json.JSONObject;

/**
 * Lesson 10 - App 1
 * <p/>
 * Created by Ing. Francesco Florio on 10/01/16
 * All Right Reserved.
 */
public class FacebookPost {

    private String id;
    private String name;
    private String picture;
    private String link;
    private String description;

    public FacebookPost(JSONObject object){
        id = object.optString("id");
        name = object.optString("name");
        picture = object.optString("full_picture");
        link = object.optString("link");
        description = object.optString("description");

        Log.i("TAG", this.toString());
    }

    public String getId() { return id; }
    public String getName() { return name; }
    public String getPicture() { return picture; }
    public String getLink() { return link; }
    public String getDescription() { return description; }

    @Override public String toString() {
        return "FacebookPost{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", full_picture='" + picture + '\'' +
                ", link='" + link + '\'' +
                ", description='" + description + '\'' +
                '}';
    }
}
