package info.francescoflorio.lesson10_app1.twitter;

import android.app.AlertDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.twitter.sdk.android.Twitter;
import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.GuestCallback;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.TwitterApiClient;
import com.twitter.sdk.android.core.TwitterCore;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.core.models.Search;
import com.twitter.sdk.android.core.models.Tweet;
import com.twitter.sdk.android.tweetui.TweetView;

import butterknife.Bind;
import butterknife.ButterKnife;
import info.francescoflorio.lesson10_app1.R;

/**
 * Lesson 10 - App 1
 * <p/>
 * Created by Ing. Francesco Florio on 10/01/16
 * All Right Reserved.
 */
public class LastTweetsActivity extends AppCompatActivity {

    private TwitterSession session;

    ListView lv;
    @Bind(R.id.userIdTV) TextView userIdTv;
    @Bind(R.id.userNameTv) TextView userNameTv;
    @Bind(R.id.accessTokenTV) TextView accessTokenTV;
    @Bind(R.id.accessTokenSecretTV) TextView accessTokenSecretTV;
    @Bind(R.id.twitterTagTv) TextView twitterTag;
    TweetAdapter adapter;
    final String TWITTER_TAG = "#homersimpson";

    @Override protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_twitterlist);
        lv = (ListView)findViewById(R.id.list);
        adapter = new TweetAdapter(getResources());
        lv.setAdapter(adapter);
        View header = LayoutInflater.from(this).inflate(R.layout.activity_twitterlist_header, null);
        ButterKnife.bind(this, header);

        session = Twitter.getSessionManager().getActiveSession();
        if(session != null) {
            userIdTv.setText(String.valueOf(session.getUserId()));
            userNameTv.setText(session.getUserName());
            accessTokenTV.setText(session.getAuthToken().token);
            accessTokenSecretTV.setText(session.getAuthToken().secret);
            twitterTag.setText(TWITTER_TAG);
        }

        lv.addHeaderView(header);
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Tweet t = (Tweet) parent.getAdapter().getItem(position);
                new AlertDialog.Builder(LastTweetsActivity.this)
                        .setView(new TweetView(LastTweetsActivity.this, t))
                        .setCancelable(true)
                        .create().show();
            }
        });
        search(TWITTER_TAG, 25);
    }

    private void search(String tag, int max){
        TwitterApiClient tClient = TwitterCore.getInstance().getApiClient(session);
        tClient.getSearchService().tweets(tag, null, null, null, null, max, null, null, null, true, new GuestCallback<>(new Callback<Search>() {
            @Override public void success(Result<Search> result) {
                adapter.updateDataset(result.data.tweets);
            }

            @Override public void failure(TwitterException exception) {
                Toast.makeText(getApplicationContext(), exception.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }));
    }
}
