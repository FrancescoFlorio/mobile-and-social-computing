package info.francescoflorio.lesson10_app1.twitter;

import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.twitter.sdk.android.core.models.Tweet;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import info.francescoflorio.lesson10_app1.R;

/**
 * Lesson 10 - App 1
 * <p/>
 * Created by Ing. Francesco Florio on 10/01/16
 * All Right Reserved.
 */
public class TweetAdapter extends BaseAdapter{

    private List<Tweet> tweets = new ArrayList<>();
    private int imageSize;

    public TweetAdapter(Resources res){
        imageSize = res.getDimensionPixelSize(R.dimen.imageSize);
    }

    @Override public int getCount() { return tweets.size(); }
    @Override public Object getItem(int position) { return tweets.get(position); }
    @Override public long getItemId(int position) { return position; }

    @Override public View getView(int position, View convertView, ViewGroup parent) {
        Context ctx = parent.getContext();
        View v = convertView;
        ViewHolder vh;
        if(v == null){
            v = LayoutInflater.from(ctx).inflate(R.layout.adapter_tweet, parent, false);
            vh = new ViewHolder(v);
            v.setTag(vh);
        }vh = (ViewHolder) v.getTag();

        Tweet t = tweets.get(position);
        vh.message.setText(t.text);
        vh.user.setText(t.user.name);
        Picasso.with(ctx)
                .load(t.user.profileImageUrl)
                .resize(imageSize, imageSize)
                .into(vh.image);

        return v;
    }

    public void updateDataset(List<Tweet> tweets){ this.tweets = tweets; notifyDataSetChanged(); }

    public class ViewHolder{
        @Bind(R.id.image) ImageView image;
        @Bind(R.id.message) TextView message;
        @Bind(R.id.user) TextView user;

        public ViewHolder(View v){
            ButterKnife.bind(this, v);
        }
    }
}
