package info.francescoflorio.lesson10_app1.twitter;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import com.twitter.sdk.android.Twitter;
import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.core.identity.TwitterLoginButton;

import butterknife.Bind;
import butterknife.ButterKnife;
import info.francescoflorio.lesson10_app1.R;

/**
 * Lesson 10 - App 1
 * <p/>
 * Created by Ing. Francesco Florio on 09/01/16
 * All Right Reserved.
 */
public class TwitterActivity extends AppCompatActivity {

    @Bind(R.id.twitter_login_button) TwitterLoginButton loginButton;

    private TwitterSession session;

    @Override protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        session = Twitter.getSessionManager().getActiveSession();
        if(session != null) openLastTweets();

        setContentView(R.layout.activity_login_twitter);
        ButterKnife.bind(this);
        loginButton.setCallback(new Callback<TwitterSession>() {
            @Override public void success(Result<TwitterSession> result) {
                TwitterSession session = result.data;
                String msg = "@" + session.getUserId() + " logged in! (#" + session.getUserName() + ")";
                Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();
                openLastTweets();
            }

            @Override public void failure(TwitterException exception) {
                Log.d("TwitterKit", "Login with Twitter failure", exception);
            }
        });

    }

    @Override protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // Make sure that the loginButton hears the result from any
        // Activity that it triggered.
        loginButton.onActivityResult(requestCode, resultCode, data);
    }

    private void openLastTweets(){
        startActivity(new Intent(getApplicationContext(), LastTweetsActivity.class));
        finish();
    }

}
