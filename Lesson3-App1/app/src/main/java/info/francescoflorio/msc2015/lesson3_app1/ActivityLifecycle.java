package info.francescoflorio.msc2015.lesson3_app1;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class ActivityLifecycle extends AppCompatActivity {

    private final String TAG = "MobileSocialComputing";
    private final String LOG_KEY = "LOG_KEY";

    protected TextView     logTextView;

    private   StringBuffer sb;
    private   DateFormat dateFormat = new SimpleDateFormat("[HH:mm:ss] ", Locale.getDefault());

    @Override protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lifecycle);
        logTextView = (TextView) findViewById(R.id.log);

        //Restore the value
        if (savedInstanceState != null) {
            String oldLog = savedInstanceState.getString(LOG_KEY, null);
            if(oldLog != null) sb = new StringBuffer(oldLog);
        }

        log(getString(R.string.state_onCreate));
    }

    @Override protected void onStart() {
        super.onStart();
        log(getString(R.string.state_onStart));
    }

    @Override protected void onResume() {
        super.onResume();
        log(getString(R.string.state_onResume));
    }

    @Override protected void onPause() {
        log(getString(R.string.state_onPause));
        super.onPause();
    }

    @Override protected void onSaveInstanceState(Bundle outState) {
        log(getString(R.string.state_onSaveInstanceState));
        outState.putString(LOG_KEY, sb.toString());
        super.onSaveInstanceState(outState);
    }

    @Override protected void onStop() {
        log(getString(R.string.state_onStop));
        super.onStop();
    }

    @Override protected void onDestroy() {
        log(getString(R.string.state_onDestroy));
        super.onDestroy();
    }

    /**
     * Clear the log
     *
     * @param view
     */
    public void onClearTapped(View view){
        Log.d(TAG, " -- on Clear Tapped");
        Toast.makeText(this, " ClearLog Tapped", Toast.LENGTH_SHORT).show();
        sb = new StringBuffer(getString(R.string.log));
        updateUi();

    }

    /**
     * Print the string to the console and to the textview if it's available
     * @param newString - The message to print
     */
    private void log(String newString){
        if(sb == null)
            sb = new StringBuffer(getString(R.string.log));
        sb.append("\n").append(dateFormat.format(new Date())).append(newString);
        Log.i(TAG, newString);
        updateUi();
    }

    private void updateUi(){
        if(logTextView != null) {
            logTextView.setText(sb.toString());
        }
    }
}
