package info.francescoflorio.msc2015.lesson3_app1;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * To work on unit tests, switch the Test Artifact in the Build Variants view.
 */
public class ExampleUnitTest {
    @Test public void addition_isCorrect() throws Exception {
        assertEquals(4, 2 + 2);
    }
}