package info.francescoflorio.msc2015.lesson3_app2;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

/**
 * Lesson 3 - App 2
 * <p/>
 * Created by Ing. Francesco Florio on 19/10/15
 * All Right Reserved.
 */
public abstract class AbstractActivity extends AppCompatActivity {

    protected final String LOG_TAG = "LOG_TAG";

    @Override protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        log(getString(R.string.state_onCreate));
    }

    @Override protected void onStart() {
        super.onStart();
        log(getString(R.string.state_onStart));
    }

    @Override protected void onResume() {
        super.onResume();
        log(getString(R.string.state_onResume));
    }

    @Override protected void onPause() {
        log(getString(R.string.state_onPause));
        super.onPause();
    }

    @Override protected void onSaveInstanceState(Bundle outState) {
        log(getString(R.string.state_onSaveInstanceState));
        super.onSaveInstanceState(outState);
    }

    @Override protected void onStop() {
        log(getString(R.string.state_onStop));
        super.onStop();
    }

    @Override protected void onDestroy() {
        log(getString(R.string.state_onDestroy));
        super.onDestroy();
    }


    /**
     * Print the string to the console
     * @param newString - The message to print
     */
    protected void log(String newString){
        Log.i(LOG_TAG, "[" + getClass().getSimpleName() + "]" + newString);
    }
}
