package info.francescoflorio.msc2015.lesson3_app2;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class FirstActivity extends AbstractActivity {
    private final String NUMBERET_KEY = "NUMBERET_KEY";

    private EditText numberET;

    @Override protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_first);
        numberET = (EditText) findViewById(R.id.number);
    }

    /**
     * Open Second Activity with the value params
     * @param view
     */
    public void submitValue(View view) {
        String valueRaw = numberET.getText().toString();
        try {
            float value = Float.parseFloat(valueRaw);

            Intent i = new Intent(this, SecondActivity.class);
            i.putExtra(SecondActivity.VALUE_KEY, value);
            startActivity(i);

        }catch (NumberFormatException nfe){
            Toast.makeText(this, getString(R.string.error_value), Toast.LENGTH_SHORT).show();
        }

    }
}
