package info.francescoflorio.msc2015.lesson3_app2;

import android.os.Bundle;
import android.widget.TextView;

public class SecondActivity extends AbstractActivity {

    public static final String VALUE_KEY = "VALUE_KEY";

    protected TextView valueLabel;

    @Override protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);
        valueLabel = (TextView) findViewById(R.id.valueLabel);

        float value = 0;
        Bundle b = getIntent().getExtras();
        if(b != null){
            value = b.getFloat(VALUE_KEY);
        }

        valueLabel.setText(getString(R.string.value_result, value));
    }

}
