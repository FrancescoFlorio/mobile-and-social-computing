package info.francescoflorio.msc2015.lesson4_app1;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;

import models.HomerStatus;

public class DynamicHomerActivity extends AppCompatActivity {

    public static final String HOMER_STATUS = "HOMER_STATUS";

    protected ImageView homerImg;

    @Override protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dynamic_homer);

        homerImg = (ImageView) findViewById(R.id.homerImg);

        Bundle b = getIntent().getExtras();
        int homerStatus = (b == null) ? HomerStatus.HAPPY
                                      : b.getInt(HOMER_STATUS, HomerStatus.HAPPY);

        switch (homerStatus){
            case HomerStatus.CRAZY: homerImg.setImageResource(R.drawable.crazy_homer); break;
            case HomerStatus.RUNNER: homerImg.setImageResource(R.drawable.runner_homer); break;
            case HomerStatus.HAPPY:
            default:
                    homerImg.setImageResource(R.drawable.happy_homer);

        }

    }
}
