package info.francescoflorio.msc2015.lesson4_app1;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class HappyHomerActivity extends AppCompatActivity {

    @Override protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_happy_homer);
    }
}
