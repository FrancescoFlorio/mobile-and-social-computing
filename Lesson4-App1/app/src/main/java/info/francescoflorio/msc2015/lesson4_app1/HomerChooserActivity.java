package info.francescoflorio.msc2015.lesson4_app1;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.RadioGroup;

import models.HomerStatus;

/**
 * Lesson 4 - App 1
 * <p/>
 * Created by Ing. Francesco Florio on 26/10/15
 * All Right Reserved.
 */
public class HomerChooserActivity extends AppCompatActivity {
    public static final String HOMER_STATUS = "HOMER_STATUS";

    protected Button confirmBtn, abortBtn;
    protected RadioGroup choices;

    @Override protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_homer_status);

        confirmBtn = (Button) findViewById(R.id.confirmBtn);
        abortBtn = (Button) findViewById(R.id.abortBtn);
        choices = (RadioGroup) findViewById(R.id.choices);

        abortBtn.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                setResult(Activity.RESULT_CANCELED);
                finish();
            }
        });

        confirmBtn.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                int status;
                switch(choices.getCheckedRadioButtonId()){
                    case R.id.homerCrazy:  status = HomerStatus.CRAZY; break;
                    case R.id.homerRunner: status = HomerStatus.RUNNER; break;
                    case R.id.homerHappy:
                    default:               status = HomerStatus.HAPPY;
                }

                Intent intent = getIntent();
                intent.putExtra(HOMER_STATUS, status);
                setResult(Activity.RESULT_OK, intent);
                finish();
            }
        });
    }
}
