package info.francescoflorio.msc2015.lesson4_app1;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.webkit.URLUtil;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.Toast;

import models.HomerStatus;

public class MainActivity extends AppCompatActivity {

    private final int SELECT_PICTURE = 100;
    private final int CHOOSE_HOMER_STATUS = 200;
    private final String ACTION_SHOW_HOMER = "info.francescoflorio.msc2015.lesson4_app1.SHOW_HOMER";

    protected Button implicitIntentButton;
    protected Button explicitIntentButton;

    protected EditText inputUrlEditText;
    protected Button   shareButton;
    protected Button   openButton;

    protected RadioGroup homerStatusRadioGroup;
    protected Button     homerOpenButton;

    protected ImageView anImage;
    protected Button    chooseImageFromGalleryBtn;

    protected ImageView statusImage;
    protected Button    chooseHomerStatusBtn;

    //Data to save
    private final String IMAGE_URI_KEY = "IMAGE_URI_KEY";
    private Uri imageUri;

    private final String STATUS_KEY = "STATUS_KEY";
    private int homerStatus = -1;

    @Override protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if(savedInstanceState != null){
            imageUri = savedInstanceState.getParcelable(IMAGE_URI_KEY);
            homerStatus = savedInstanceState.getInt(STATUS_KEY, -1);
        }
/////////////////////////////

        explicitIntentButton = (Button) findViewById(R.id.openIntentExplicit);
        explicitIntentButton.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, HappyHomerActivity.class);
                startActivity(intent);
            }
        });

        implicitIntentButton = (Button) findViewById(R.id.openIntentImplicit);
        implicitIntentButton.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                Intent intent = new Intent(ACTION_SHOW_HOMER);
                startActivity(intent);
            }
        });

/////////////////////////////

        inputUrlEditText = (EditText)findViewById(R.id.inputUrl);
        shareButton = (Button) findViewById(R.id.actionOpenBtn);
        openButton = (Button) findViewById(R.id.actionShareBtn);

        View.OnClickListener urlClickListener = new View.OnClickListener() {
            @Override public void onClick(View v) {
                String value = inputUrlEditText.getText().toString();

                if(URLUtil.isValidUrl(value)){
                    Intent intent = null;
                    if(v.getId() == R.id.actionOpenBtn) {
                        intent = new Intent(Intent.ACTION_VIEW, Uri.parse(value));
                    }else if(v.getId() == R.id.actionShareBtn){
                        intent = new Intent(Intent.ACTION_SEND);
                        intent.putExtra(Intent.EXTRA_TEXT, value);
                        intent.setType("text/plain");
                    }
                    startActivity(intent);
                }else
                    Toast.makeText(MainActivity.this,
                                   getString(R.string.error_url_not_valid),
                                   Toast.LENGTH_SHORT).show();
            }
        };
        shareButton.setOnClickListener(urlClickListener);
        openButton.setOnClickListener(urlClickListener);

/////////////////////////////

        homerStatusRadioGroup = (RadioGroup)findViewById(R.id.homerStatus);
        homerOpenButton = (Button)findViewById(R.id.openHomerStatusBtn);
        homerOpenButton.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, DynamicHomerActivity.class);
                int homerStatus;
                switch (homerStatusRadioGroup.getCheckedRadioButtonId()){
                    case R.id.homerCrazy: homerStatus = HomerStatus.CRAZY; break;
                    case R.id.homerRunner: homerStatus = HomerStatus.RUNNER; break;
                    case R.id.homerHappy:
                    default: homerStatus = HomerStatus.HAPPY; break;
                }
                intent.putExtra(DynamicHomerActivity.HOMER_STATUS, homerStatus);
                startActivity(intent);
            }
        });

/////////////////////////////
        anImage = (ImageView) findViewById(R.id.galleryImage);
        if(imageUri != null)
            anImage.setImageURI(imageUri);

        chooseImageFromGalleryBtn = (Button) findViewById(R.id.chooseImageBtn);
        chooseImageFromGalleryBtn.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(intent, SELECT_PICTURE);
            }
        });
/////////////////////////////
        statusImage = (ImageView) findViewById(R.id.statusImage);
        if(homerStatus > -1)
            statusImage.setImageResource(getResIdFromStatus(homerStatus));
        chooseHomerStatusBtn = (Button) findViewById(R.id.statusChooserBtn);
        chooseHomerStatusBtn.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, HomerChooserActivity.class);
                startActivityForResult(intent, CHOOSE_HOMER_STATUS);
            }
        });


    }

    private int getResIdFromStatus(int status){
        switch (status){
            case HomerStatus.CRAZY: return R.drawable.crazy_homer;
            case HomerStatus.RUNNER: return R.drawable.runner_homer;
            case HomerStatus.HAPPY:
            default: return R.drawable.happy_homer;
        }
    }


    @Override public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == SELECT_PICTURE) {
            if (resultCode == RESULT_OK) {
                imageUri = data.getData();
                anImage.setImageURI(imageUri);
            }
        }
        else if(requestCode == CHOOSE_HOMER_STATUS){
            if(resultCode == RESULT_OK){
                homerStatus = data.getIntExtra(HomerChooserActivity.HOMER_STATUS, 0);
                statusImage.setImageResource(getResIdFromStatus(homerStatus));
            }else if(resultCode == RESULT_CANCELED){
                Toast.makeText(this, R.string.no_choice, Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override protected void onSaveInstanceState(Bundle outState) {
        if(imageUri != null) outState.putParcelable(IMAGE_URI_KEY, imageUri);
        if(homerStatus > -1) outState.putInt(STATUS_KEY, homerStatus);
        super.onSaveInstanceState(outState);
    }
}
