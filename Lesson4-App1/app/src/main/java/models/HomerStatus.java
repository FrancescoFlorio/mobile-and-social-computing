package models;

/**
 * Lesson 4 - App 1
 * <p/>
 * Created by Ing. Francesco Florio on 26/10/15
 * All Right Reserved.
 */
public class HomerStatus {
    public static final int HAPPY = 0,
                            CRAZY = 1,
                            RUNNER = 2;
}
