package com.ssd.rssreader;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import org.parceler.Parcels;

public class DetailActivity extends Activity {

	public static final String NEWS = "news";
	
	private News currNews;
	
	@Override protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.details_view);
		
		currNews = Parcels.unwrap(getIntent().getParcelableExtra(NEWS));
		
		TextView title = (TextView) findViewById(R.id.title);
		TextView description = (TextView) findViewById(R.id.description);
		TextView pubdate = (TextView) findViewById(R.id.pubdate);
		
		Button link = (Button)findViewById(R.id.link);
		
		title.setText(currNews.getTitle());
		description.setText(currNews.getDescription());
		pubdate.setText(currNews.getPubDate());
		
		link.setOnClickListener(new View.OnClickListener() {
			@Override public void onClick(View v) {
				Intent myIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(currNews.getLink()));
				startActivity(myIntent);
			}
		});
	}

}
