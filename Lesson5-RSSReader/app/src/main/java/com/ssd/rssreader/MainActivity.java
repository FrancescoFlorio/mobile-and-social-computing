package com.ssd.rssreader;

import java.util.ArrayList;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import org.parceler.Parcels;

public class MainActivity extends Activity{

	protected RSSListAdapter adapter;
	protected ArrayList<News> items;
	protected RSSDownloadInterface callback = new RSSDownloadInterface() {
		@Override public void onNewsDownloaded(ArrayList<News> newItems) {
			items.clear();
			for(News n: newItems)
				items.add(n);
			adapter.notifyDataSetChanged();
		}
	};
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		items = new ArrayList<>();
		adapter = new RSSListAdapter(this, items);
		
		ListView rssList = (ListView) findViewById(R.id.rssList);
		rssList.setEmptyView(findViewById(android.R.id.empty));
		rssList.addHeaderView(View.inflate(this, R.layout.list_header, null));
		rssList.setAdapter(adapter);
		rssList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				News n = items.get(position-1);
				Intent i = new Intent(MainActivity.this, DetailActivity.class);
				i.putExtra(DetailActivity.NEWS, Parcels.wrap(n));
				startActivity(i);	
			}
		});
		
		updateNews();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.activity_main, menu);
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch(item.getItemId()){
			 case (R.id.menu_reload): {
				 		updateNews();
				 		return true;
			 			}
		}
		return false;
	}

	public void updateNews(){
		RSSAsyncTask task = new RSSAsyncTask(this, callback);
		task.execute(getString(R.string.rss_url));
	}
	
}
