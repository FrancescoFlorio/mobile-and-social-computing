package com.ssd.rssreader;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.os.AsyncTask;

public class RSSAsyncTask extends AsyncTask<String, Void, ArrayList<News>> {

	private Activity currActivity;
	private ProgressDialog pd;
	private RSSDownloadInterface callback;
	
	public RSSAsyncTask(Activity activity, RSSDownloadInterface callback){
		this.currActivity = activity;
		this.callback = callback;
	}
	
	
	@Override protected void onPreExecute() {
		super.onPreExecute();
		pd = ProgressDialog.show(	currActivity, 
									null, 
									currActivity.getString(R.string.loadingTxt),
									true,
									true,
									new OnCancelListener(){
										@Override public void onCancel(DialogInterface dialog) {
											RSSAsyncTask.this.cancel(true);
											pd.dismiss();
										}
									}
								);
	}
	
	@Override protected ArrayList<News> doInBackground(String... arg0) {
		ArrayList<News> items = new ArrayList<>();
				
		try {
			URL rssUrl = new URL(arg0[0]);
			SAXParserFactory mySAXParserFactory = SAXParserFactory.newInstance();
			SAXParser mySAXParser = mySAXParserFactory.newSAXParser();
			XMLReader myXMLReader = mySAXParser.getXMLReader();
			RSSHandler myRSSHandler = new RSSHandler();
			myXMLReader.setContentHandler(myRSSHandler);
			InputSource myInputSource = new InputSource(rssUrl.openStream());
			myXMLReader.parse(myInputSource);

			items.addAll(myRSSHandler.getFeed());

		} 
		catch (MalformedURLException e) { } 
		catch (ParserConfigurationException e) {} 
		catch (SAXException e) {} 
		catch (IOException e) {}
				
		return items;
	}

	@Override protected void onPostExecute(ArrayList<News> result) {
		super.onPostExecute(result);
		pd.dismiss();
		callback.onNewsDownloaded(result);
	}

	

	
	
}
