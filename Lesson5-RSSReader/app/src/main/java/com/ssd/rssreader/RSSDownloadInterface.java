package com.ssd.rssreader;

import java.util.ArrayList;

public interface RSSDownloadInterface {

	void onNewsDownloaded(ArrayList<News> items);
	
}
