package com.ssd.rssreader;

import java.util.ArrayList;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class RSSHandler extends DefaultHandler {

	final int state_unknown = 0;
	final int state_title = 1;
	final int state_description = 2;
	final int state_link = 3;
	final int state_pubdate = 4;
	int currentState = state_unknown;

	ArrayList<News> items;
	News item;

	boolean itemFound = false;

	public RSSHandler(){}

	public ArrayList<News> getFeed(){ return items; }

	@Override public void startDocument() throws SAXException {
		items = new ArrayList<News>();
		item = new News();

	}

	@Override public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
		
		if (localName.equalsIgnoreCase("item")){
			itemFound = true;
			item = new News();
			currentState = state_unknown;
		}
		else if (localName.equalsIgnoreCase("title")){
			currentState = state_title;
		}
		else if (localName.equalsIgnoreCase("description")){
			currentState = state_description;
		}
		else if (localName.equalsIgnoreCase("link")){
			currentState = state_link;
		}
		else if (localName.equalsIgnoreCase("pubdate")){
			currentState = state_pubdate;
		}
		else{
			currentState = state_unknown;
		}

	}

	@Override public void endElement(String uri, String localName, String qName) throws SAXException {
		if (localName.equalsIgnoreCase("item")){
			items.add(item);
		}
	}

	@Override public void characters(char[] ch, int start, int length) throws SAXException {
		String strCharacters = new String(ch,start,length);

		if (itemFound==true){
			// "item" tag found, it's item's parameter
			switch(currentState){
			case state_title:
				item.setTitle(strCharacters);
				break;
			case state_description:
				item.setDescription(strCharacters);
				break;
			case state_link:
				item.setLink(strCharacters);
				break;
			case state_pubdate:
				item.setPubDate(strCharacters);
				break;
			default:
				break;
			}
		}
		currentState = state_unknown;
	}

	@Override public void endDocument() throws SAXException { /* do nothing */}

}
