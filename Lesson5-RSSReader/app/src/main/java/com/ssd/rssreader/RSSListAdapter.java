package com.ssd.rssreader;

import java.util.ArrayList;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class RSSListAdapter extends BaseAdapter {

	protected Context ctx;
	protected ArrayList<News> items;
	
	public RSSListAdapter(Context ctx, ArrayList<News> items){
		this.ctx = ctx;
		this.items = items;
	}
	
	@Override public int getCount() { return items.size(); }
	@Override public Object getItem(int position) { return items.get(position); }
	@Override public long getItemId(int position) { return position; }
	
	@Override public View getView(int position, View convertView, ViewGroup parent){
		View v = convertView;
		if(v == null)
			v = View.inflate(ctx, R.layout.list_item, null);

		final News currNews = items.get(position);
		if(currNews != null){
			final TextView title = (TextView)v.findViewById(R.id.title);
			final TextView subtitle = (TextView)v.findViewById(R.id.subtitle);
			
			title.setText(currNews.getTitle());
			subtitle.setText(currNews.getPubDate());
		}
		
		return v;
	}
}
