package info.francescoflorio.lesson6_app1;

import android.content.SharedPreferences;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private final String PREFS_NAME = "SIMPLE_PREFS";
    private final String OWNER_KEY = "OWNER";

    private EditText ownerEt;
    private TextView labelTv;
    private Button saveBtn, clearBtn;

    private SharedPreferences sharedPreferences;

    @Override protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        labelTv = (TextView)findViewById(R.id.label);
        ownerEt = (EditText)findViewById(R.id.owner);
        saveBtn = (Button)findViewById(R.id.save);
        clearBtn = (Button)findViewById(R.id.clear);

        sharedPreferences = getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        String name = sharedPreferences.getString(OWNER_KEY, "");

        if(name.isEmpty()) updateUiWithoutName();
        else updateUiWithName(name);

        saveBtn.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                String newName = ownerEt.getText().toString();
                if(newName.isEmpty()){
                    ownerEt.setError(getText(R.string.error__name_required));
                }else{
                    sharedPreferences.edit()
                                     .putString(OWNER_KEY, newName)
                                     .apply();

                    updateUiWithName(newName);
                }
            }
        });

        clearBtn.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                sharedPreferences.edit()
                                 .clear()
                                 .apply();

                updateUiWithoutName();
            }
        });
    }

    private void updateUiWithName(String name){
        saveBtn.setText(R.string.edit);
        labelTv.setText(getString(R.string.owner_name, name));
        labelTv.setTextColor(Color.BLUE);
        clearBtn.setEnabled(true);
        ownerEt.setText("");
    }

    private void updateUiWithoutName(){
        saveBtn.setText(R.string.save);
        labelTv.setText(R.string.no_owner);
        labelTv.setTextColor(Color.GRAY);
        clearBtn.setEnabled(false);
        ownerEt.setText("");
    }

}
