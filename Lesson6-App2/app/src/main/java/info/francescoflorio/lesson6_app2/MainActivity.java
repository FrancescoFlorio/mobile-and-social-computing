package info.francescoflorio.lesson6_app2;
import android.os.AsyncTask;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.RadioGroup;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import info.francescoflorio.lesson6_app2.adapter.ListAdapter;
import info.francescoflorio.lesson6_app2.model.Teammate;

public class MainActivity extends AppCompatActivity {

    private RadioGroup radioGroup;
    private ListView list;
    private ListAdapter adapter;

    private String datasetCopy;

    @Override protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        list = (ListView) findViewById(R.id.list);
        adapter = new ListAdapter();
        list.setAdapter(adapter);

        radioGroup = (RadioGroup) findViewById(R.id.chooser);
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override public void onCheckedChanged(RadioGroup group, int checkedId) {
                new LoadTeamAsyncTask().execute(getCurrentInputStream(checkedId));
            }
        });

        Button copyBtn = (Button)findViewById(R.id.copyBtn);
        copyBtn.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                new SaveAsyncTask().execute(getCurrentInputStream());
            }
        });

        new LoadTeamAsyncTask().execute(getCurrentInputStream());
    }

    @Nullable private InputStream getCurrentInputStream(){
        int checkedId = (radioGroup.getCheckedRadioButtonId());
        return getCurrentInputStream(checkedId);
    }

    @Nullable private InputStream getCurrentInputStream(int checkedId){
        InputStream is = null;
        if(checkedId == R.id.simpsons) {
            try { is = getResources().getAssets().open("simpson.json");}
            catch (IOException ioe) { Log.e("myApp", ioe.getMessage()); }
        }else if(checkedId == R.id.team)
            is = getResources().openRawResource(R.raw.team);
        return is;
    }

    private void updateUi(List<Teammate> teammates){
        adapter.updateWithData(teammates);
    }

    class LoadTeamAsyncTask extends AsyncTask<InputStream, Void, List<Teammate>>{
         @Override protected List<Teammate> doInBackground(InputStream... params) {
             List<Teammate> dataset = new ArrayList<>();
             if(params.length < 1 || params[0] == null) return dataset;

             StringBuilder jsonSb = new StringBuilder();
             try {
                 BufferedInputStream resourceStream = new BufferedInputStream(params[0]);
                 BufferedReader reader = new BufferedReader(new InputStreamReader(resourceStream));
                 String line;
                 while((line = reader.readLine()) != null)
                     jsonSb.append(line);
                 reader.close();
                 resourceStream.close();
             } catch(Exception ex) { Log.e("myApp", ex.getMessage()); }

             try {
                 datasetCopy = jsonSb.toString();
                 JSONArray jsonArray = new JSONArray(datasetCopy);
                 for(int i = 0; i<jsonArray.length(); i++)
                     dataset.add(Teammate.createFromJson(jsonArray.getJSONObject(i)));

             }catch (JSONException jsoe){ Log.e("myApp", jsoe.getMessage()); }
             return dataset;
        }

        @Override protected void onPostExecute(List<Teammate> teammates) {
            updateUi(teammates);
        }
    }

    class SaveAsyncTask extends AsyncTask<InputStream, Void, Boolean>{
        @Override protected Boolean doInBackground(InputStream... params) {
            if(params.length < 1 || params[0] == null) return false;
            InputStream in = params[0];
            File defaultFolder = Environment.getExternalStorageDirectory();
            File copyFile = new File(defaultFolder, "copy.json");
            try {
                FileOutputStream fos = new FileOutputStream(copyFile);
                byte[] b = new byte[8];
                int i;
                while ((i = in.read(b)) != -1) {
                    fos.write(b, 0, i);
                }
                fos.flush();
                fos.close();
                in.close();
            }catch (Exception e){
                Log.e("myApp", e.getMessage());
                return false;
            }

            return true;
        }

        @Override protected void onPostExecute(Boolean result) {
            Toast.makeText(getApplicationContext(), (result) ? "File copied!" : "File not copied! An error occurred!", Toast.LENGTH_SHORT).show();
        }
    }
}
