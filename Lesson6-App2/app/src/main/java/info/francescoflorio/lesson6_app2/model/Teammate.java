package info.francescoflorio.lesson6_app2.model;

import org.json.JSONObject;

/**
 * Lesson6 - App2
 * <p/>
 * Created by Ing. Francesco Florio on 16/11/15
 * All Right Reserved.
 */
public class Teammate {

    private static final String NAME = "name",
                                AGE = "age",
                                ROLE = "role";

    private String name;
    private int age;
    private String role;

    public Teammate(){}

    public String getName() { return name; }
    public void setName(String name) { this.name = name; }
    public int getAge() { return age; }
    public void setAge(int age) { this.age = age; }
    public String getRole() { return role; }
    public void setRole(String role) { this.role = role; }

    public static Teammate createFromJson(JSONObject jsonObject){
        Teammate teammate = new Teammate();

        teammate.setName(jsonObject.optString(NAME));
        teammate.setAge(jsonObject.optInt(AGE));
        teammate.setRole(jsonObject.optString(ROLE));

        return teammate;
    }

}
