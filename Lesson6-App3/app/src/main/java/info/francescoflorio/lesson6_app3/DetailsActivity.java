package info.francescoflorio.lesson6_app3;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import org.parceler.Parcels;

import java.util.List;

import info.francescoflorio.lesson6_app3.db.DatabaseManger;
import info.francescoflorio.lesson6_app3.db.Teammate;

public class DetailsActivity extends AppCompatActivity {

    public static final String TEAMMATE = "TEAMMATE";

    private EditText nameEt;
    private EditText ageEt;
    private EditText roleEt;
    private Button saveBtn;

    private DatabaseManger dbManager;

    @Override protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);

        nameEt = (EditText)findViewById(R.id.name);
        ageEt = (EditText)findViewById(R.id.age);
        roleEt = (EditText)findViewById(R.id.role);
        saveBtn = (Button)findViewById(R.id.save);

        Bundle b = getIntent().getExtras();
        final int id;
        if(b != null){
            Teammate t = Parcels.unwrap(b.getParcelable(TEAMMATE));
            id = t.getId();
            nameEt.setText(t.getName());
            ageEt.setText(String.valueOf(t.getAge()));
            roleEt.setText(t.getRole());
        }else id = -1;

        dbManager = new DatabaseManger(getApplicationContext());
        dbManager.open();

        saveBtn.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                int age = 0;
                try{ age = Integer.parseInt(ageEt.getText().toString());}
                catch (NumberFormatException nfe){}
                Teammate t = new Teammate(id,
                                          nameEt.getText().toString(),
                                          age,
                                          roleEt.getText().toString());
                new SaveAsyncTask().execute(t);
            }
        });

    }

    @Override protected void onDestroy() {
        dbManager.close();
        super.onDestroy();
    }

    private void closeAndReturn(Teammate t){
        if(t == null)
            setResult(RESULT_CANCELED);
        else {
            Intent i = getIntent();
            i.putExtra(TEAMMATE, Parcels.wrap(t));
            setResult(RESULT_OK, i);
        }
        finish();
    }

    class SaveAsyncTask extends AsyncTask<Teammate, Void, Teammate> {
        @Override protected Teammate doInBackground(Teammate... params) {
            if(params.length < 1 || params[0] == null) return null;
            if(params[0].getId() < 0)
                dbManager.addTeammate(params[0]);
            else
                dbManager.editTeammate(params[0]);

            return params[0];
        }

        @Override protected void onPostExecute(Teammate teammate) {
            closeAndReturn(teammate);
        }
    }
}
