package info.francescoflorio.lesson6_app3;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;

import org.parceler.Parcels;

import java.util.ArrayList;
import java.util.List;

import info.francescoflorio.lesson6_app3.adapter.ListAdapter;
import info.francescoflorio.lesson6_app3.db.DatabaseManger;
import info.francescoflorio.lesson6_app3.db.Teammate;

public class MainActivity extends AppCompatActivity {

    private ListView list;
    private Button addBtn;

    private ListAdapter adapter;
    private DatabaseManger dbManager;

    @Override protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        list = (ListView)findViewById(R.id.list);
        addBtn = (Button)findViewById(R.id.addBtn);

        adapter = new ListAdapter();
        dbManager = new DatabaseManger(getApplicationContext());
        dbManager.open();

        list.setAdapter(adapter);
        new LoadTeamAsyncTask().execute();

        addBtn.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                openDetails(null);
            }
        });

        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Teammate t = (Teammate) parent.getAdapter().getItem(position);
                openDetails(t);
            }
        });

        list.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                Teammate t = (Teammate) parent.getAdapter().getItem(position);
                new DeleteAndReloadAsyncTask().execute(t);
                return true;
            }
        });
    }

    @Override protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode == RESULT_OK)
            new LoadTeamAsyncTask().execute();
    }

    @Override protected void onDestroy() {
        dbManager.close();
        super.onDestroy();
    }

    private void updateUi(List<Teammate> teammates){
        adapter.updateWithData(teammates);
    }

    private void openDetails(Teammate t){
        Intent i = new Intent(this, DetailsActivity.class);
        if(t != null)
            i.putExtra(DetailsActivity.TEAMMATE, Parcels.wrap(t));
        startActivityForResult(i, 100);
    }



    class LoadTeamAsyncTask extends AsyncTask<Void, Void, List<Teammate>> {
        @Override protected List<Teammate> doInBackground(Void... params) {
            return dbManager.getAllTeammate();
        }

        @Override protected void onPostExecute(List<Teammate> teammates) {
            updateUi(teammates);
        }
    }


    class DeleteAndReloadAsyncTask extends AsyncTask<Teammate, Void, List<Teammate>> {
        @Override protected List<Teammate> doInBackground(Teammate... params) {
            if(params.length < 1 || params[0] == null) return null;
            if(params[0].getId() <= 0) return null;

            dbManager.deleteTeammate(params[0]);
            return dbManager.getAllTeammate();
        }

        @Override protected void onPostExecute(List<Teammate> teammates) {
            updateUi(teammates);
        }
    }

}
