package info.francescoflorio.lesson6_app3.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import info.francescoflorio.lesson6_app3.db.Teammate;

/**
 * Lesson6 - App2
 * <p/>
 * Created by Ing. Francesco Florio on 16/11/15
 * All Right Reserved.
 */
public class ListAdapter extends BaseAdapter {

    private List<Teammate> dataset = new ArrayList<>();

    @Override public int getCount() { return dataset.size(); }
    @Override public Object getItem(int position) { return dataset.get(position); }
    @Override public long getItemId(int position) { return position; }
    @Override public View getView(int position, View convertView, ViewGroup parent) {
        Context ctx = parent.getContext();
        ViewHolder viewHolder;
        View v = convertView;
        if(v == null){
            v = LayoutInflater.from(ctx).inflate(android.R.layout.simple_list_item_activated_2, parent, false);
            viewHolder = new ViewHolder();
            viewHolder.tv1 = (TextView) v.findViewById(android.R.id.text1);
            viewHolder.tv2 = (TextView) v.findViewById(android.R.id.text2);
            v.setTag(viewHolder);
        }else viewHolder = (ViewHolder) v.getTag();

        Teammate t = dataset.get(position);
        viewHolder.tv1.setText(t.getName()+" ("+t.getAge()+")");
        viewHolder.tv2.setText(t.getRole());

        return v;
    }

    public void updateWithData(List<Teammate> newDataset){
        dataset.clear();
        dataset.addAll(newDataset);
        notifyDataSetChanged();
    }

    class ViewHolder{
        TextView tv1, tv2;
    }
}
