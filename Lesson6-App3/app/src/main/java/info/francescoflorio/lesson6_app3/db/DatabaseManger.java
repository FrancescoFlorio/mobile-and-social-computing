package info.francescoflorio.lesson6_app3.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.provider.BaseColumns;

import java.util.ArrayList;
import java.util.List;

/**
 * Lesson 6 - App 3
 * <p/>
 * Created by Ing. Francesco Florio on 16/11/15
 * All Right Reserved.
 */
public class DatabaseManger {
/*
    private static DatabaseManger instance;

    public static DatabaseManger getInstance(Context ctx){
        if(instance == null) instance = new DatabaseManger(ctx);
        return instance;
    }
*/
    // Database fields
    private SQLiteDatabase database;
    private MySQLiteHelper dbHelper;
    private String[] allColumns = { BaseColumns._ID,
                                    Teammate.COLUMN_NAME,
                                    Teammate.COLUMN_AGE,
                                    Teammate.COLUMN_ROLE };

    public DatabaseManger(Context context) { dbHelper = new MySQLiteHelper(context); }

    public void open() throws SQLException {
        if(database == null)
            database = dbHelper.getWritableDatabase();
    }

    public void close() {
        dbHelper.close();
    }

    public List<Teammate> getAllTeammate(){
        List<Teammate> teammates = new ArrayList<>();

        /*
         * Query the given table, returning a {@link Cursor} over the result set.
         *
         * @param table The table name to compile the query against.
         * @param columns A list of which columns to return. Passing null will
         *            return all columns, which is discouraged to prevent reading
         *            data from storage that isn't going to be used.
         * @param selection A filter declaring which rows to return, formatted as an
         *            SQL WHERE clause (excluding the WHERE itself). Passing null
         *            will return all rows for the given table.
         * @param selectionArgs You may include ?s in selection, which will be
         *         replaced by the values from selectionArgs, in order that they
         *         appear in the selection. The values will be bound as Strings.
         * @param groupBy A filter declaring how to group rows, formatted as an SQL
         *            GROUP BY clause (excluding the GROUP BY itself). Passing null
         *            will cause the rows to not be grouped.
         * @param having A filter declare which row groups to include in the cursor,
         *            if row grouping is being used, formatted as an SQL HAVING
         *            clause (excluding the HAVING itself). Passing null will cause
         *            all row groups to be included, and is required when row
         *            grouping is not being used.
         * @param orderBy How to order the rows, formatted as an SQL ORDER BY clause
         *            (excluding the ORDER BY itself). Passing null will use the
         *            default sort order, which may be unordered.
         */
        Cursor cursor = database.query(Teammate.TABLE_TEAM,
                                        allColumns,
                                        null,
                                        null,
                                        null,
                null, Teammate.COLUMN_AGE + " ASC");

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            teammates.add(Teammate.buildFromCursor(cursor));
            cursor.moveToNext();
        }
        return teammates;
    }

    public void addTeammate(Teammate t){
        ContentValues values = t.getContentValues();
        long insertId = database.insert(Teammate.TABLE_TEAM, null, values);
    }

    public void editTeammate(Teammate t){
        ContentValues values = t.getContentValues();
        database.update(Teammate.TABLE_TEAM, values, BaseColumns._ID +" = ?", new String[]{String.valueOf(t.getId())});
    }

    public void deleteTeammate(Teammate t){
        database.delete(Teammate.TABLE_TEAM, BaseColumns._ID +" = ?", new String[]{String.valueOf(t.getId())});
    }
}
