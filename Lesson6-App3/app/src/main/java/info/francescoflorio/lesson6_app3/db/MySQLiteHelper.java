package info.francescoflorio.lesson6_app3.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;
import android.util.Log;


/**
 * Lesson 6 - App 3
 * <p/>
 * Created by Ing. Francesco Florio on 16/11/15
 * All Right Reserved.
 */
public class MySQLiteHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "dataset.db";
    private static final int DATABASE_VERSION = 1;


    // Database creation sql statement
    private static final String CREATE_TEAM_TABLE = "create table "
            + Teammate.TABLE_TEAM +
                "(" + BaseColumns._ID + " integer primary key autoincrement, " +
                      Teammate.COLUMN_NAME + " text not null, " +
                      Teammate.COLUMN_AGE + " integer not null, " +
                      Teammate.COLUMN_ROLE + " text not null"
             + " );";

    public MySQLiteHelper(Context context) { super(context, DATABASE_NAME, null, DATABASE_VERSION); }

    @Override public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TEAM_TABLE);
    }

    @Override public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.w(MySQLiteHelper.class.getName(), "Upgrading database from version " + oldVersion + " to " + newVersion + ", which will destroy all old data");
        db.execSQL("DROP TABLE IF EXISTS " + Teammate.TABLE_TEAM);
        onCreate(db);
    }
}
