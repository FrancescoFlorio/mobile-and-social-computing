package info.francescoflorio.lesson6_app3.db;

import android.content.ContentValues;
import android.database.Cursor;

import org.parceler.Parcel;

/**
 * Lesson 6 - App 3
 * <p/>
 * Created by Ing. Francesco Florio on 16/11/15
 * All Right Reserved.
 */

@Parcel
public class Teammate {

    public static final String TABLE_TEAM = "team";


    public static final String COLUMN_NAME = "name";
    public static final String COLUMN_AGE = "age";
    public static final String COLUMN_ROLE = "role";

    public int id;
    public String name;
    public int age;
    public String role;

    public Teammate(){}
    public Teammate(int id, String name, int age, String role) {
        this.id = id;
        this.name = name;
        this.age = age;
        this.role = role;
    }

    public int getId() { return id; }
    public void setId(int id) { this.id = id; }
    public String getName() { return name; }
    public void setName(String name) { this.name = name; }
    public int getAge() { return age; }
    public void setAge(int age) { this.age = age; }
    public String getRole() { return role; }
    public void setRole(String role) { this.role = role; }


// DATABASE UTILS

    public static Teammate buildFromCursor(Cursor c){
        return new Teammate(c.getInt(0),
                            c.getString(1),
                            c.getInt(2),
                            c.getString(3));
    }

    public ContentValues getContentValues(){
        ContentValues values = new ContentValues();
        values.put(COLUMN_NAME, name);
        values.put( COLUMN_AGE, age);
        values.put(COLUMN_ROLE, role);
        return values;
    }
}
