package info.francescoflorio.lesson8_app2;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 * Lezione 8 - App 1
 * <p/>
 * Created by Ing. Francesco Florio on 30/11/15
 * All Right Reserved.
 */
public class DetailsFragment extends Fragment {

    private  static final String ITEM_ORDINAL = "ITEM_ORDINAL";

    private FamilyItems item;


    public static DetailsFragment getInstance(FamilyItems item){
        DetailsFragment df = new DetailsFragment();
        Bundle b = new Bundle();
        b.putInt(ITEM_ORDINAL, item.ordinal());
        df.setArguments(b);
        return df;
    }

    protected TextView label;

    @Override public void onCreate(Bundle savedInstanceState) {
        Log.d("L8A2", "  >>>> Fragment for: "+this.toString()+" created!");
        super.onCreate(savedInstanceState);
    }
    @Override public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_details, container, false);
    }

    @Override public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        label = (TextView)view.findViewById(R.id.label);
        Bundle bundle = getArguments();
        if(bundle != null){
            int ordinal = bundle.getInt(ITEM_ORDINAL, 0);
            item = FamilyItems.values()[ordinal];
            updateItem(item);
        }

        Log.d("L8A2", "  >> Fragment for: '"+item.getName()+"' view created into "+this.toString());
    }


    @Override public void onDestroyView() {
        Log.d("L8A2", "  << Fragment for: '"+item.getName()+"' view destroyed from "+this.toString());
        super.onDestroyView();
    }

    @Override public void onDestroy() {
        Log.d("L8A2", "  <<<< Fragment for: " +this.toString()+" destroyed!");
        super.onDestroy();
    }

    public void updateItem(FamilyItems item){
        label.setText(item.getName());
        label.setCompoundDrawablesWithIntrinsicBounds(0, item.getImageResId(), 0, 0);
    }
}
