package info.francescoflorio.lesson8_app2;

import android.support.annotation.DrawableRes;

/**
 * Lezione 8 - App 1
 * <p/>
 * Created by Ing. Francesco Florio on 30/11/15
 * All Right Reserved.
 */
public enum FamilyItems {
    HOMER("Homer Simpson", R.drawable.homer_simpson),
    MARGE("Marge Simpson", R.drawable.marge_simpson),
    BART("Bart Simpson", R.drawable.bart_simpson),
    LISA("Lisa Simpson", R.drawable.lisa_simpson),
    MAGGIE("Maggie Simpson", R.drawable.maggie_simpson),
    ABRAHAM("Abraham Simpson", R.drawable.abraham_simpson),
    HERBERT("Herbert Powell", R.drawable.herbert_powell),
    PATTY("Patty Bouvier", R.drawable.patty_bouvier),
    SELMA("Selma Bouvier", R.drawable.selma_bouvier);

    private String name;
    private int imageResId;

    private FamilyItems(String name, @DrawableRes int imageResId){
        this.name = name;
        this.imageResId = imageResId;
    }

    public String getName() { return name; }
    public int getImageResId() { return imageResId; }

    @Override public String toString() {
        return name;
    }
}
