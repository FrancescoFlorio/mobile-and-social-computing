package info.francescoflorio.lesson8_app2;

import android.app.Fragment;
import android.app.FragmentManager;
import android.support.v13.app.FragmentPagerAdapter;
import android.view.ViewGroup;

/**
 * Lesson8 - App 2
 * <p/>
 * Created by Ing. Francesco Florio on 30/11/15
 * All Right Reserved.
 */
public class SimpsonViewPagerAdapter extends FragmentPagerAdapter {

    private final FamilyItems[] items = FamilyItems.values();

    public SimpsonViewPagerAdapter(FragmentManager fm){ super(fm); }


    @Override public Fragment getItem(int position) {
        return DetailsFragment.getInstance(items[position]);
    }

    @Override public int getCount() {
        return items.length;
    }
    @Override public CharSequence getPageTitle(int position) { return items[position].toString(); }
}
