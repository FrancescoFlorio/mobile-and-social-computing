package info.francescoflorio.lesson8_app3;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Lesson 8 - App 3
 * <p/>
 * Created by Ing. Francesco Florio on 30/11/15
 * All Right Reserved.
 */
public class DummyFragment extends Fragment {

    @Override public void onCreate(Bundle savedInstanceState) {
        Log.d("L8A3", "    [F] onCreate ");
        super.onCreate(savedInstanceState);
    }

    @Override public void onAttach(Activity activity) {
        Log.d("L8A3", "    [F] onAttach ");
        super.onAttach(activity);
    }

    @Override public View onCreateView(LayoutInflater inflater, ViewGroup container,  Bundle savedInstanceState) {
        Log.d("L8A3", "    [F] onCreateView ");
        return inflater.inflate(R.layout.dummy_fragment, container, false);
    }

    @Override public void onViewCreated(View view, Bundle savedInstanceState) {
        Log.d("L8A3", "    [F] onViewCreated ");
        super.onViewCreated(view, savedInstanceState);
    }

    @Override public void onResume() {
        Log.d("L8A3", "    [F] onResume ");
        super.onResume();
    }

    @Override public void onStart() {
        Log.d("L8A3", "    [F] onStart ");
        super.onStart();
    }

    @Override public void onStop() {
        Log.d("L8A3", "    [F] onStop ");
        super.onStop();
    }

    @Override public void onPause() {
        Log.d("L8A3", "    [F] onPause ");
        super.onPause();
    }

    @Override public void onDetach() {
        Log.d("L8A3", "    [F] onDetach ");
        super.onDetach();
    }

    @Override public void onDestroyView() {
        Log.d("L8A3", "    [F] onDestroyView ");
        super.onDestroyView();
    }

    @Override public void onDestroy() {
        Log.d("L8A3", "    [F] onDestroy ");
        super.onDestroy();
    }
}
