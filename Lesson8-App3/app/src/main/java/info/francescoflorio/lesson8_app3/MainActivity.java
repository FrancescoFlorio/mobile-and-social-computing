package info.francescoflorio.lesson8_app3;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

public class MainActivity extends AppCompatActivity {

    @Override protected void onCreate(Bundle savedInstanceState) {
        Log.d("L8A3", " [A] onCreate ");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override protected void onResume() {
        Log.d("L8A3", " [A] onResume ");
        super.onResume();
    }

    @Override protected void onStart() {
        Log.d("L8A3", " [A] onStart ");
        super.onStart();
    }

    @Override protected void onStop() {
        Log.d("L8A3", " [A] onStop ");
        super.onStop();
    }

    @Override protected void onPause() {

        Log.d("L8A3", " [A] onPause ");
        super.onPause();
    }

    @Override protected void onDestroy() {
        Log.d("L8A3", " [A] onDestroy ");
        super.onDestroy();
    }
}
