package info.francescoflorio.lesson8_app4;

import android.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.RadioGroup;

public class MainActivity extends AppCompatActivity {

    protected RadioGroup radioGroup;
    protected FamilyItems currItem;

    @Override protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        radioGroup = (RadioGroup) findViewById(R.id.radioGroup);

        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override public void onCheckedChanged(RadioGroup group, int checkedId) {
                FamilyItems newItem = null;
                if(checkedId == R.id.homerBtn) newItem = FamilyItems.HOMER;
                else if(checkedId == R.id.margeBtn) newItem = FamilyItems.MARGE;
                else if(checkedId == R.id.bartBtn) newItem = FamilyItems.BART;
                if(newItem != null)
                    updateNewStatus(newItem);
            }
        });
        updateNewStatus(FamilyItems.HOMER);
    }

    private void updateNewStatus(FamilyItems newItem){
        if(newItem.equals(currItem)) return;
        currItem = newItem;
        FragmentManager fm = getFragmentManager();
        fm.beginTransaction()
                .replace(R.id.container, SimpsonFragment.getInstance(currItem))
//                .addToBackStack(null)
                .commit();
    }

    @Override public void onBackPressed() {
        int count = getFragmentManager().getBackStackEntryCount();
        if(count > 1) getFragmentManager().popBackStack();
        else super.onBackPressed();
    }
}
