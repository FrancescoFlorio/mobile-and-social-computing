package info.francescoflorio.lesson9_app1;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import info.francescoflorio.lesson9_app1.adapters.ResultAdapter;
import info.francescoflorio.lesson9_app1.model.Result;
import info.francescoflorio.lesson9_app1.model.SearchResult;
import info.francescoflorio.lesson9_app1.networking.NetworkManager;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

public class MainActivity extends AppCompatActivity {

    @Bind(R.id.search) EditText searchEt;
    @Bind(R.id.searchBtn) Button searchBtn;
    @Bind(R.id.result) ListView resultList;

    protected ResultAdapter adapter;
    protected NetworkManager networkManager;

    @Override protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        networkManager = ((MyApplication)getApplication()).getNetworkManager();
        adapter = new ResultAdapter();
        resultList.setAdapter(adapter);
    }

    @OnClick(R.id.searchBtn) void onSearch(){
        String txt = searchEt.getText().toString();
        networkManager.performSearch(txt, new Callback<SearchResult>() {
            @Override public void onResponse(Response<SearchResult> response, Retrofit retrofit) {
                SearchResult searchResult = response.body();
                adapter.updateDataset(searchResult.results);
            }
            @Override public void onFailure(Throwable t) {
                Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }


}
