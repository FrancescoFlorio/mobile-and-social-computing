package info.francescoflorio.lesson9_app1;

import android.app.Application;

import info.francescoflorio.lesson9_app1.networking.NetworkManager;

/**
 * Lesson9 - App1
 * <p/>
 * Created by Ing. Francesco Florio on 14/12/15
 * All Right Reserved.
 */
public class MyApplication extends Application {

    private NetworkManager networkManager;

    @Override public void onCreate() {
        super.onCreate();
    }

    public NetworkManager getNetworkManager(){
        if(networkManager == null)
            networkManager = new NetworkManager(getApplicationContext());
        return networkManager;
    }
}
