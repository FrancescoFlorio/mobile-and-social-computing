package info.francescoflorio.lesson9_app1.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import info.francescoflorio.lesson9_app1.R;
import info.francescoflorio.lesson9_app1.model.Result;

/**
 * Lesson9 - App1
 * <p/>
 * Created by Ing. Francesco Florio on 14/12/15
 * All Right Reserved.
 */
public class ResultAdapter extends BaseAdapter {

    List<Result> results = new ArrayList<>();

    @Override public int getCount() { return results.size(); }
    @Override public Object getItem(int position) { return results.get(position);  }
    @Override public long getItemId(int position) {  return position; }

    @Override public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder vh;
        Context context = parent.getContext();
        View v = convertView;
        if(v == null){
            v = LayoutInflater.from(context).inflate(R.layout.adapter_result, parent, false);
            vh = new ViewHolder(v);
            v.setTag(vh);
        }else vh = (ViewHolder)v.getTag();

        Result result = results.get(position);
        vh.trackName.setText(result.trackName);
        vh.artistName.setText(result.artistName);
        Picasso.with(context)
               .load(result.artwork)
                .resize(40, 40)
                .error(android.R.drawable.picture_frame)
               .into(vh.thumb);
        return v;
    }

    public void updateDataset(List<Result> newDataset){
        results.clear();
        if(newDataset != null)
            results.addAll(newDataset);
        notifyDataSetChanged();
    }

    public class ViewHolder{
        @Bind(R.id.thumb) public ImageView thumb;
        @Bind(R.id.trackName) public TextView trackName;
        @Bind(R.id.artistName) public TextView artistName;

        public ViewHolder(View v){
            ButterKnife.bind(this, v);
        }
    }
}
