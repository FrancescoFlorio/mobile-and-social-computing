package info.francescoflorio.lesson9_app1.model;

import com.google.gson.annotations.SerializedName;

/**
 * Lesson9 - App1
 * <p/>
 * Created by Ing. Francesco Florio on 14/12/15
 * All Right Reserved.
 */
public class Result {

    @SerializedName("trackName") public String trackName;
    @SerializedName("artistName") public String artistName;
    @SerializedName("artworkUrl100") public String artwork;

    @Override public String toString() {
        return "Result{" +
                "trackName='" + trackName + '\'' +
                ", artistName='" + artistName + '\'' +
                ", artwork='" + artwork + '\'' +
                '}';
    }
}
