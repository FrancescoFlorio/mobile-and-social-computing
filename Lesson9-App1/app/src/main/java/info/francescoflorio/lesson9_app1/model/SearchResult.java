package info.francescoflorio.lesson9_app1.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Lesson9 - App1
 * <p/>
 * Created by Ing. Francesco Florio on 14/12/15
 * All Right Reserved.
 */
public class SearchResult {

    @SerializedName("resultCount") public int resultCount;
    @SerializedName("results") public List<Result> results;
}
