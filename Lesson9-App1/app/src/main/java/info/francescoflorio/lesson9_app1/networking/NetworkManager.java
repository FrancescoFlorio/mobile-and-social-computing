package info.francescoflorio.lesson9_app1.networking;

import android.content.Context;

import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.logging.HttpLoggingInterceptor;

import java.util.Locale;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import info.francescoflorio.lesson9_app1.BuildConfig;
import info.francescoflorio.lesson9_app1.R;
import info.francescoflorio.lesson9_app1.model.SearchResult;
import retrofit.Call;
import retrofit.Callback;
import retrofit.GsonConverterFactory;
import retrofit.Retrofit;

/**
 * Lesson9 - App1
 * <p/>
 * Created by Ing. Francesco Florio on 14/12/15
 * All Right Reserved.
 */
public class NetworkManager {
    protected  Context ctx;
    protected  RestServices services;

    public NetworkManager(Context ctx){
        this.ctx = ctx;

        OkHttpClient okHttpClient = new OkHttpClient();
        okHttpClient.setRetryOnConnectionFailure(true);
        okHttpClient.setConnectTimeout(30, TimeUnit.SECONDS);
        okHttpClient.setReadTimeout(30, TimeUnit.SECONDS);

        if(BuildConfig.DEBUG) {
            HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            okHttpClient.interceptors().add(interceptor);
        }

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(getBaseUrl())
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        services = retrofit.create(RestServices.class);
    }

    private String getBaseUrl(){
        return ctx.getString(R.string.baseUrl);
    }

    public void performSearch(String query, Callback<SearchResult> callback){
        Call<SearchResult> callable = services.performSearch(query, Locale.getDefault().getCountry());
        callable.enqueue(callback);
    }


}
