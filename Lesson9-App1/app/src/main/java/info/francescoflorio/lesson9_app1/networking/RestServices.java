package info.francescoflorio.lesson9_app1.networking;

import info.francescoflorio.lesson9_app1.model.SearchResult;
import retrofit.Call;
import retrofit.http.GET;
import retrofit.http.Query;

/**
 * Lesson9 - App1
 * <p/>
 * Created by Ing. Francesco Florio on 14/12/15
 * All Right Reserved.
 */
public interface RestServices {

    String TERM = "term";
    String COUNTRY = "country";

    @GET("/search")
    Call<SearchResult> performSearch(@Query(TERM) String term, @Query(COUNTRY) String locale);
}
