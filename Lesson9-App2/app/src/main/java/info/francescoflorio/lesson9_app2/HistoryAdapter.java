package info.francescoflorio.lesson9_app2;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import info.francescoflorio.lesson9_app2.model.Message;

/**
 * Lesson9 - App 2
 * <p/>
 * Created by Ing. Francesco Florio on 15/12/15
 * All Right Reserved.
 */
public class HistoryAdapter extends BaseAdapter {

    private List<Message> dataset = new ArrayList<>();

    @Override public int getCount() { return dataset.size(); }
    @Override public Object getItem(int position) { return dataset.get(position); }
    @Override public long getItemId(int position) { return position; }
    @Override public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder vh;
        Context ctx = parent.getContext();
        View v = convertView;
        if(v == null){
            v = LayoutInflater.from(ctx).inflate(android.R.layout.simple_list_item_2, parent, false);
            vh = new ViewHolder(v);
            v.setTag(vh);
        }else vh = (ViewHolder)v.getTag();

        Message m = dataset.get(position);
        vh.text1.setText(m.getTitle());
        vh.text2.setText(m.getMessage());
        return v;

    }

    public void updateDataset(List<Message> newDataset){
        dataset.clear();
        if(newDataset != null) dataset.addAll(newDataset);
        notifyDataSetChanged();
    }

    public class ViewHolder{
        @Bind(android.R.id.text1) TextView text1;
        @Bind(android.R.id.text2) TextView text2;

        public ViewHolder(View v){
            ButterKnife.bind(this, v);
        }
    }
}
