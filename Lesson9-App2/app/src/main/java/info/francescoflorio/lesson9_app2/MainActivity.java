package info.francescoflorio.lesson9_app2;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.ListView;

import com.parse.DeleteCallback;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseQuery;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnItemLongClick;
import info.francescoflorio.lesson9_app2.model.Message;

public class MainActivity extends AppCompatActivity {

    @Bind(R.id.coordinatorLayout) CoordinatorLayout coordinatorLayout;
    @Bind(R.id.toolbar) Toolbar toolbar;
    @Bind(R.id.historyList) ListView historyList;

    protected HistoryAdapter adapter;

    @Override protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        adapter = new HistoryAdapter();
        historyList.setAdapter(adapter);
        historyList.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                Message m = (Message) adapter.getItem(position);
                m.deleteInBackground(new DeleteCallback() {
                    @Override public void done(ParseException e) {
                        reloadData();
                        Snackbar.make(coordinatorLayout, "Message removed!", Snackbar.LENGTH_SHORT).show();
                    }
                });
                return true;
            }
        });

        reloadData();

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View view) {
                Intent i = new Intent(getApplicationContext(), SendActivity.class);
                startActivityForResult(i, SendActivity.REQUEST_SEND);
            }
        });
    }

    @Override protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == SendActivity.REQUEST_SEND && resultCode == Activity.RESULT_OK){
            reloadData();
            Snackbar.make(coordinatorLayout, "Message sent!", Snackbar.LENGTH_SHORT).show();
        }
        else super.onActivityResult(requestCode, resultCode, data);
    }


    private void reloadData(){
        ParseQuery<Message> query = new ParseQuery<>("Message");
        query.findInBackground(new FindCallback<Message>() {
            @Override public void done(List<Message> objects, ParseException e) {
                adapter.updateDataset(objects);
            }
        });
    }


}
