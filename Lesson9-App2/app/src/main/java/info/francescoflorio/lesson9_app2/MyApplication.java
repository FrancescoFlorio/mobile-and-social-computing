package info.francescoflorio.lesson9_app2;

import android.app.Application;

import com.parse.Parse;
import com.parse.ParseACL;
import com.parse.ParseObject;

import info.francescoflorio.lesson9_app2.model.Message;

/**
 * Lesson9 - App 2
 * <p/>
 * Created by Ing. Francesco Florio on 14/12/15
 * All Right Reserved.
 */
public class MyApplication extends Application {

    @Override public void onCreate() {
        super.onCreate();

        ParseObject.registerSubclass(Message.class);

        Parse.enableLocalDatastore(this);
        Parse.initialize(getApplicationContext(),
                getString(R.string.parse_app_id),
                getString(R.string.parse_client_key));

        ParseACL defaultACL = new ParseACL();
        defaultACL.setPublicReadAccess(true);
        defaultACL.setPublicWriteAccess(true);
        ParseACL.setDefaultACL(defaultACL, true);

    }
}
