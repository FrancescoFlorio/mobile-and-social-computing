package info.francescoflorio.lesson9_app2;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;
import android.widget.EditText;

import com.parse.ParseException;
import com.parse.SaveCallback;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import info.francescoflorio.lesson9_app2.model.Message;

/**
 * Lesson9 - App 2
 * <p/>
 * Created by Ing. Francesco Florio on 14/12/15
 * All Right Reserved.
 */
public class SendActivity extends AppCompatActivity {

    public static final int REQUEST_SEND = 232;

    @Bind(R.id.title) EditText titleEt;
    @Bind(R.id.message) EditText messageEt;
    @Bind(R.id.sendBtn) Button sendBtn;

    @Override protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_send);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.sendBtn) void onSend(){
        Message message = new Message();
        message.setTitle(titleEt.getText().toString());
        message.setMessage(messageEt.getText().toString());
        message.saveInBackground(new SaveCallback() {
            @Override public void done(ParseException e) {
                setResult(Activity.RESULT_OK);
                finish();
            }
        });

    }


}
