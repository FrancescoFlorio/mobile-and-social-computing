package info.francescoflorio.lesson9_app2.model;

import com.parse.ParseClassName;
import com.parse.ParseObject;

/**
 * Lesson9 - App 2
 * <p/>
 * Created by Ing. Francesco Florio on 15/12/15
 * All Right Reserved.
 */

@ParseClassName("Message")
public class Message extends ParseObject{
    public static final String KEY_TITLE = "title";
    public static final String KEY_MESSAGE = "message";

    public Message(){}

    public String getTitle(){ return getString(KEY_TITLE); }
    public String getMessage(){ return getString(KEY_MESSAGE); }

    public void setTitle(String title){
        if(title != null) put(KEY_TITLE, title);
        else remove(KEY_TITLE);
    }

    public void setMessage(String message){
        if(message != null) put(KEY_MESSAGE, message);
        else remove(KEY_MESSAGE);
    }

}
