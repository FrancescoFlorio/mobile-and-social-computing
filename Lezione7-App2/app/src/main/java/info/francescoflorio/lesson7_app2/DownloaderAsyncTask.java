package info.francescoflorio.lesson7_app2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;

public class DownloaderAsyncTask extends AsyncTask<String, Void, ArrayList<MallBean>> {

	private final String positionsKey = "positions";
	private final String latKey = "lat";
	private final String lngKey = "lng";
	private final String nameKey = "name";
	
	
	private Activity currActivity;
	private ProgressDialog pd;
	private DownloaderCallbackInterface callback;
	
	public DownloaderAsyncTask(Activity activity, DownloaderCallbackInterface callback){
		this.currActivity = activity;
		this.callback = callback;
	}

	@Override protected void onPreExecute() {
		super.onPreExecute();
		pd = ProgressDialog.show(	currActivity, 
									null, 
									currActivity.getString(R.string.loadingTxt),
									true,
									true,
									new OnCancelListener() {
										@Override public void onCancel(DialogInterface dialog) {
											DownloaderAsyncTask.this.cancel(true);
											pd.dismiss();
										}
									}
								);
	}
	
	@Override
	protected ArrayList<MallBean> doInBackground(String... params) {
		
		try{
			Thread.sleep(5*1000);
		}catch(InterruptedException ie){}
		
		ArrayList<MallBean> items = new ArrayList<>();
		if(params.length > 0){
			try{	
				HttpURLConnection conn = (HttpURLConnection) new URL(params[0]).openConnection();
				InputStream is = conn.getInputStream();
				BufferedReader reader = new BufferedReader(new InputStreamReader(is));
				StringBuilder sb = new StringBuilder();

				String content = null;
				while ((content = reader.readLine()) != null)
					sb.append(content + "\n");
				
				content = sb.toString();
				
				is.close();
				conn.disconnect();
				
				JSONObject json = new JSONObject(content);
				if(json != null){
					if(!json.isNull(positionsKey)){
						JSONArray positions = json.getJSONArray(positionsKey);
						JSONObject currObject;
						for(int i=0; i<positions.length(); i++){
							currObject = positions.getJSONObject(i);
							MallBean ccb = new MallBean();
							if(!currObject.isNull(latKey))
								ccb.setLat(currObject.getDouble(latKey));
							if(!currObject.isNull(lngKey))
								ccb.setLng(currObject.getDouble(lngKey));
							if(!currObject.isNull(nameKey))
								ccb.setName(currObject.getString(nameKey));
								
							items.add(ccb);	
						}
					}
				}
				
			}
			catch(IOException ioe){}
			catch(JSONException jsone){}
			
			
		}
		
		return items;
	}
	
	@Override protected void onPostExecute(ArrayList<MallBean> result) {
		super.onPostExecute(result);
		pd.dismiss();
		callback.onDownloadTaskCompleted(result);
	}

}
