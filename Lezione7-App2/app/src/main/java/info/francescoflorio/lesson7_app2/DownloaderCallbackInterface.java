package info.francescoflorio.lesson7_app2;

import java.util.ArrayList;

public interface DownloaderCallbackInterface {
	
	public void onDownloadTaskCompleted(ArrayList<MallBean> items);
	
}
