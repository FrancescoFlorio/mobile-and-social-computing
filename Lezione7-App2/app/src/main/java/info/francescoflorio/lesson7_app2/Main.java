package info.francescoflorio.lesson7_app2;

import java.util.ArrayList;

import android.location.Location;
import android.os.Bundle;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import org.parceler.Parcels;

public class Main extends AppCompatActivity implements
		GoogleApiClient.ConnectionCallbacks,
		GoogleApiClient.OnConnectionFailedListener{
	
	
	private ArrayList<MallBean> items;
	
	// UI
	private TextView currPosition;
	private Button openMap;
	private ListView mallList;
	private PositionsAdapter adapter;
	
	//GPS
	private double myLat, myLng;
	protected boolean mRequestingLocationUpdates = true;

	protected LocationRequest mLocationRequest;
	protected GoogleApiClient mGoogleApiClient;

	private LocationListener locationListener = new LocationListener() {
		public void onLocationChanged(Location location) {
			updateDataAndUi(location);
			mRequestingLocationUpdates = false;
			stopLocationService();
		}

		public void onStatusChanged(String provider, int status, Bundle extras) {}
		public void onProviderEnabled(String provider) {}
		public void onProviderDisabled(String provider) {}
	};

    @Override protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        items = new ArrayList<MallBean>();

        currPosition = (TextView) findViewById(R.id.currPosition);
        openMap = (Button) findViewById(R.id.goToMap);
        mallList = (ListView)findViewById(R.id.otherPositions);
        adapter = new PositionsAdapter(this, items);
        mallList.setAdapter(adapter);

		createLocationRequest();
		buildGoogleApiClient();
        executeAsyncTask();
        
        openMap.setOnClickListener(new View.OnClickListener() {
			@Override public void onClick(View arg0) {
				Intent i = new Intent(Main.this, MyMapActivity.class);
				i.putExtra(MyMapActivity.ITEMS, Parcels.wrap(items));
				i.putExtra(MyMapActivity.MY_LAT, myLat);
				i.putExtra(MyMapActivity.MY_LNG, myLng);
				startActivity(i);
			}
		});
    }

	@Override public void onResume() {
		super.onResume();
		if (mGoogleApiClient.isConnected() && !mRequestingLocationUpdates)
			startLocationUpdates();
	}

	@Override protected void onPause() {
		super.onPause();
		stopLocationService();
	}

	@Override public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.activity_main, menu);
        return true;
    }
    
    @Override public boolean onOptionsItemSelected(MenuItem item) {
		switch(item.getItemId()){
			 case (R.id.menu_reload): {
				 		executeAsyncTask();
				 		return true;
			 			}
		}
		return false;
	}

///////////////////////////////////////////////////////
// Interfaces
///////////////////////////////////////////////////////
	@Override public void onConnectionSuspended(int i) {}

	@Override public void onConnected(Bundle bundle) {
		Log.d("MY_APP", "on API connected...");
		Location mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
		if (mLastLocation != null)
			updateDataAndUi(mLastLocation);

		if (mRequestingLocationUpdates) {
			startLocationUpdates();
		}
	}

	@Override public void onConnectionFailed(ConnectionResult connectionResult) {
		Toast.makeText(this, "Error: "+connectionResult.getErrorMessage(), Toast.LENGTH_SHORT).show();
	}

///////////////////////////////////////////////////////
// utility methods
///////////////////////////////////////////////////////
	protected synchronized void buildGoogleApiClient() {
		mGoogleApiClient = new GoogleApiClient.Builder(this)
				.addConnectionCallbacks(this)
				.addOnConnectionFailedListener(this)
				.addApi(LocationServices.API)
				.build();
		mGoogleApiClient.connect();
		Log.d("MY_APP", "buildGoogleApiClient...");

	}

	@Override protected void onDestroy() {
		if(mGoogleApiClient != null && mGoogleApiClient.isConnected())
			mGoogleApiClient.disconnect();
		super.onDestroy();
	}

	protected void createLocationRequest() {
		mLocationRequest = new LocationRequest();
		mLocationRequest.setInterval(10000);
		mLocationRequest.setFastestInterval(5000);
		mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
	}
    
    private void executeAsyncTask(){
    	DownloaderAsyncTask atask = new DownloaderAsyncTask(this, new DownloaderCallbackInterface() {
			@Override public void onDownloadTaskCompleted(ArrayList<MallBean> newItems) {
				items.clear();
				for(MallBean ccb : newItems)
					items.add(ccb);
				
				adapter.notifyDataSetChanged();
			}
		});
    	atask.execute("https://dl.dropboxusercontent.com/u/4193235/malls.txt");
    }

	private void updateDataAndUi(Location location){
		myLat = location.getLatitude();
		myLng =  location.getLongitude();

		//Aggiorno l'UI
		String label = getString(R.string.currPosition, myLat, myLng, location.getAccuracy());
		currPosition.setText(label);

		//aggiorno il riferimento nell'adapter
		adapter.setCurrLocation(location);
	}

	protected void startLocationUpdates() {
		LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, locationListener);
	}

	private void stopLocationService() {
		LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, locationListener);
	}
}
