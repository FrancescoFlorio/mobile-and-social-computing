package info.francescoflorio.lesson7_app2;


import org.parceler.Parcel;

@Parcel
public class MallBean {

	private String name;
	private double lat;
	private double lng;
	
	public MallBean(){}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public double getLat() {
		return lat;
	}
	public void setLat(double lat) {
		this.lat = lat;
	}
	public double getLng() {
		return lng;
	}
	public void setLng(double lng) {
		this.lng = lng;
	}
	
	@Override public String toString() {
		return "MallBean [name=" + name + ", lat=" + lat
				+ ", lng=" + lng + "]";
	}
	
	
}
