package info.francescoflorio.lesson7_app2;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.parceler.Parcels;

import java.util.ArrayList;
import java.util.List;

public class MyMapActivity extends FragmentActivity implements OnMapReadyCallback {

	private GoogleMap mMap;

	public static final String ITEMS = "items";
	public static final String MY_LAT = "myLat";
	public static final String MY_LNG = "myLng";

	private ArrayList<MallBean> items;
	private double myLat, myLng;
	
	@SuppressWarnings("unchecked")
	@Override public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.map_layout);

		SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
		mapFragment.getMapAsync(this);
	}

	  /*
	    List<Overlay> mapOverlays = mapView.getOverlays();
	    Drawable myDrawable = this.getResources().getDrawable(R.drawable.my_androidmarker);
	    Drawable defaultDrawable = this.getResources().getDrawable(R.drawable.androidmarker);
	    MyItemizedOverlay itemizedoverlay = new MyItemizedOverlay(this, myDrawable, defaultDrawable,items, myLat, myLng);
	    mapOverlays.add(itemizedoverlay);
	}
	
	@Override protected boolean isRouteDisplayed() {
		// TODO Auto-generated method stub
		return false;
	}
	*/

	@Override public void onMapReady(GoogleMap googleMap) {
		mMap = googleMap;
		Intent i = getIntent();

		items = Parcels.unwrap(i.getParcelableExtra(ITEMS));
		myLat = i.getDoubleExtra(MY_LAT, 0);
		myLng = i.getDoubleExtra(MY_LNG, 0);
		addMarkersToMap();
	}

	private void addMarkersToMap() {
		mMap.clear();
		LatLng ll;
		BitmapDescriptor bitmapMarker = BitmapDescriptorFactory.defaultMarker();
		LatLngBounds.Builder b = new LatLngBounds.Builder();

		List<Marker> mMarkers = new ArrayList<>(items.size()+1);
		for (MallBean mall : items) {
			ll = new LatLng(mall.getLat(), mall.getLng());

			mMarkers.add(mMap.addMarker(new MarkerOptions().position(ll)
														   .title(mall.getName())
					 								       .icon(bitmapMarker)));
			b.include(ll);
		}

		bitmapMarker = BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN);
		ll = new LatLng(myLat, myLng);
		mMarkers.add(mMap.addMarker(new MarkerOptions().position(ll)
				.icon(bitmapMarker)));

		b.include(ll);
		LatLngBounds bounds = b.build();

		//Change the padding as per needed
		CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, 5);
		mMap.animateCamera(cu);
	}

}
