package info.francescoflorio.lesson7_app2;

import java.util.ArrayList;

import android.content.Context;
import android.location.Location;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class PositionsAdapter extends BaseAdapter {

	private Context c;
	private ArrayList<MallBean> items;
	private Location currLocation;
	
	public PositionsAdapter(Context c, ArrayList<MallBean> items){
		this.c = c;
		this.items = items;
	}
	
	@Override public int getCount() { return items.size(); }
	@Override public Object getItem(int position) { return items.get(position);}
	@Override public long getItemId(int position) { return position; }
	@Override public View getView(int position, View convertView, ViewGroup parent) {
		View v = convertView;
		if(v == null)
			v = View.inflate(c, R.layout.list_item, null);
		
		MallBean currItem = items.get(position);
		if(currItem != null){
			final TextView name = (TextView) v.findViewById(R.id.name);
			final TextView gpsPosition = (TextView) v.findViewById(R.id.gpsPosition);
			final TextView distance = (TextView) v.findViewById(R.id.distance);
			
			if(currLocation != null){
				float[] results = new float[3];
				Location.distanceBetween( currLocation.getLatitude(),
										  currLocation.getLongitude(), 
										  currItem.getLat(), 
										  currItem.getLng(), results);
				if(results[0] > 0){
					String distanceTxt = c.getString(R.string.distance, results[0]);
					distance.setText(distanceTxt);
				}
			}

			name.setText(currItem.getName());
			String gpsPositionTxt = c.getString(R.string.gpsPosition, currItem.getLat(), currItem.getLng());
			gpsPosition.setText(gpsPositionTxt);
			
		}
		return v;
	}

	public void setCurrLocation(Location loc){
		currLocation = loc;
	}
}
