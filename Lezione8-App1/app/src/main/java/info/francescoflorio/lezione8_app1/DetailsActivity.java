package info.francescoflorio.lezione8_app1;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

/**
 * Lezione 8 - App 1
 * <p/>
 * Created by Ing. Francesco Florio on 30/11/15
 * All Right Reserved.
 */
public class DetailsActivity extends AppCompatActivity {

    public static final String ITEM_ORDINAL = "ITEM_ORDINAL";

    @Override public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);
    }
}
