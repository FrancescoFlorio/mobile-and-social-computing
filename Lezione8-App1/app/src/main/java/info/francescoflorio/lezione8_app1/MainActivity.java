package info.francescoflorio.lezione8_app1;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

import info.francescoflorio.lezione8_app1.fragments.DetailsFragment;
import info.francescoflorio.lezione8_app1.interfaces.MasterActivityDelegate;

public class MainActivity extends AppCompatActivity implements MasterActivityDelegate {

    private final String LAST_FAMILY_ITEM = "LAST_FAMILY_ITEM";

    protected boolean isSmartphone;
    protected FamilyItems lastFamilyItem = FamilyItems.HOMER;
    private DetailsFragment df;

    @Override protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        isSmartphone = getResources().getBoolean(R.bool.isSmartphone);
        if(savedInstanceState != null)
            lastFamilyItem = FamilyItems.values()[savedInstanceState.getInt(LAST_FAMILY_ITEM, 0)];

        if(!isSmartphone)
            changeFragment(lastFamilyItem);
    }

    @Override public void onItemTapped(FamilyItems item) {
        if(isSmartphone) openNewActivity(item);
        else changeFragment(item);
    }

    private void openNewActivity(FamilyItems item){
        Intent i = new Intent(this, DetailsActivity.class);
        i.putExtra(DetailsActivity.ITEM_ORDINAL, item.ordinal());
        startActivity(i);
    }

    @Override protected void onSaveInstanceState(Bundle outState) {
        outState.putInt(LAST_FAMILY_ITEM, lastFamilyItem.ordinal());
        super.onSaveInstanceState(outState);
    }
    private void changeFragment(FamilyItems item){
        lastFamilyItem = item;
        if(df ==  null)
            df = (DetailsFragment) getFragmentManager().findFragmentByTag(getString(R.string.details_tag));
        df.updateItem(item);
    }


}
