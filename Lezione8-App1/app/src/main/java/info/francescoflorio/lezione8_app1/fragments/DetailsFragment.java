package info.francescoflorio.lezione8_app1.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import info.francescoflorio.lezione8_app1.DetailsActivity;
import info.francescoflorio.lezione8_app1.FamilyItems;
import info.francescoflorio.lezione8_app1.R;

/**
 * Lezione 8 - App 1
 * <p/>
 * Created by Ing. Francesco Florio on 30/11/15
 * All Right Reserved.
 */
public class DetailsFragment extends Fragment {
/*
    public static DetailsFragment getInstance(FamilyItems item){
        DetailsFragment df = new DetailsFragment();
        Bundle b = new Bundle();
        b.putInt(DetailsActivity.ITEM_ORDINAL, item.ordinal());
        df.setArguments(b);
        return df;
    }
*/
    protected TextView label;

    @Override public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_details, container, false);
    }

    @Override public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        label = (TextView)view.findViewById(R.id.label);
        Bundle bundle = getActivity().getIntent().getExtras();
        if(bundle != null){
            int ordinal = bundle.getInt(DetailsActivity.ITEM_ORDINAL, 0);
            FamilyItems item = FamilyItems.values()[ordinal];
            updateItem(item);
        }
    }

    public void updateItem(FamilyItems item){
        label.setText(item.getName());
        label.setCompoundDrawablesWithIntrinsicBounds(0, item.getImageResId(), 0, 0);
    }
}
