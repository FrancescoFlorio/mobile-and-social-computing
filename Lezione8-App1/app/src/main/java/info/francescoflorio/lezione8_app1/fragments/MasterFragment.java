package info.francescoflorio.lezione8_app1.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import info.francescoflorio.lezione8_app1.FamilyItems;
import info.francescoflorio.lezione8_app1.R;
import info.francescoflorio.lezione8_app1.interfaces.MasterActivityDelegate;

/**
 * Lezione 8 - App 1
 * <p/>
 * Created by Ing. Francesco Florio on 30/11/15
 * All Right Reserved.
 */
public class MasterFragment extends Fragment {


    @Override public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_master, container, false);
    }

    @Override public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        final ListView list = (ListView)view.findViewById(R.id.list);
        final ArrayAdapter<FamilyItems> adapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_list_item_1, FamilyItems.values());
        list.setAdapter(adapter);

        if(getActivity() instanceof MasterActivityDelegate){
            list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    ((MasterActivityDelegate) getActivity()).onItemTapped(adapter.getItem(position));
                }
            });
        }
    }
}
