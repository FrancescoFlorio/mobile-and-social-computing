package info.francescoflorio.lezione8_app1.interfaces;

import info.francescoflorio.lezione8_app1.FamilyItems;

/**
 * Lezione 8 - App 1
 * <p/>
 * Created by Ing. Francesco Florio on 30/11/15
 * All Right Reserved.
 */
public interface MasterActivityDelegate {
    void onItemTapped(FamilyItems item);
}
